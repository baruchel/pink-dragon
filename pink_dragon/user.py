# -*- coding: utf-8 -*-

from random import randrange, choice, random
from time import time



class User:
    def __init__(self, game, nick):
        self.nick = nick
        self.creation_time = time()
        self.game = game
        self.network = game.network
        self.money = 0
        self.weapon = None
        self.weapon_count = 0 # number of fights with the same weapon
        self.proficiency_bonus = 0
        self.armor = None
        self.idle_clock = 0
        self.level = 1
        self.xp = 0
        self.strength = 15
        self.dexterity = 14
        self.hp = 10 # at each new level: add 1d10 or 6   + constitution modifier
        self.ac = 12 # 10 + modifier(dexterity = 14)
        # valeur standard: force, dextérité, constitution : 15, 14, 13
        self.quest_win = 0
        self.quest_lost = 0
        self.strategy = lambda m: random()

    def test_level(self):
        if self.xp >= 355000: return 20
        for i, x in enumerate(
               [ 0, 300, 900, 2700, 6500, 14000, 23000, 34000,
                 48000, 64000, 85000, 100000, 120000, 140000,
                 165000, 195000, 225000, 265000, 305000, 355000 ]):
            if x > self.xp: return i

    def level_up(self):
        """
        Test if character needs ot level up and does it.
        Return the number of gained levels (generally 0 or 1)
        """
        new = self.test_level()
        delta = new - self.level
        self.level = new
        for _ in range(delta):
            # change HP either 6 or 1d10
            # the game will choose 6 with prob. 2/3 or 1d10 with prob. 1/3
            base = 6 if randrange(0,3) < 2 else randrange(1, 11)
            # we add a bonus +2 coming from:
            #  * bonus +1 (racial constitution)
            #  * bonus +1 related to average constitution = 13
            self.hp += base + 2
            self.strength += 1 # theoretically max=20
            self.dexterity += 1 # theoretically max=20
            if self.armor == None: self.ac = 10 + (self.dexterity-10)//2
        if delta:
            self.network.broadcast(
                self.network.code_bold + self.nick
              + self.network.code_reset + " has gained "
              + str(delta) + " level"
              + ("s!" if delta > 1 else "!")
              + " New level is " + str(self.level) + "." 
              + " New HP is " + str(self.hp) + "." )
        return delta

    def give_xp(self, xp):
        """
        Give some XP to the player.
        Return the number of gained levels (generally 0 or 1)
        """
        self.xp += xp
        self.network.broadcast(
            self.network.code_bold + self.nick
          + self.network.code_reset + " has gained "
          + str(xp) + " XP! (Total XP = " + str(self.xp)
          + ", current money = " + str(self.money) + ").")
        return self.level_up()

    def compute_proficiency(self):
        """
        Add 1 to self.weapon_count and see if weapon is now mastered.
        Set self.proficiency_bonus if required
        """
        if self.weapon == None:
            self.proficiency_bonus = 0
            return False
        self.weapon_count += 1
        if self.proficiency_bonus: return False
        # We choose to give the proficiency bonus according to the cost
        # of the weapon and weight:
        #  * expensive weapons are more difficult to use.
        #  * heavy weapons are more difficult to use.
        cost = self.weapon["cost"]     # between 10 and 5000
        weight = self.weapon["weight"] # between 1 and 18
        # required count is between 8 and 45 with:
        #      cost.bit_length() * weight.bit_length()
        # adding an extra >> 1 makes it between 4 and 22
        required_count = (cost.bit_length() * weight.bit_length()) >> 1
        if self.weapon_count >= required_count:
            self.proficiency_bonus = (self.level + 7) >> 2
        if self.weapon_count == required_count:
            self.network.broadcast(
                    self.network.code_bold + self.nick +
                    self.network.code_reset + " has now a proficiency"
                         + " bonus (+" + str(self.proficiency_bonus)
                         + ") when using a " + self.weapon["name"]  + "!" )


    class Warrior:
        def __init__(self, hp, attacks, dexterity, armor_class, cr,
                     strategy):
            self.hp = hp
            self.attacks = attacks
            self.dexterity = dexterity
            self.dext_mod = (dexterity-10) // 2
            self.ac = armor_class
            self.cr = cr
            self.strategy = strategy

    @staticmethod
    def build_warrior(user, strict=False):
        """
        Build a warrior (object involved in fighting) from a user.
        In order to match the exact gameplay, the warrior is enhanced
        with some ad-hoc tunings when 'strict==False'.
        """
        w = user.Warrior( user.hp, [ {
                            "attack_bonus": (user.strength - 10)//2
                                           + user.proficiency_bonus,
                            "damage_dice": (
                    lambda: ( sum( randrange(1, user.weapon["damage"]["dice_value"]+1)
                              for _ in range(user.weapon["damage"]["dice_count"]) )
                              if user.weapon else 0 )),
                            "damage_bonus": (user.strength - 10)//2
                        } ], user.dexterity, user.ac, user.level,
                        user.strategy)
        if strict: return w

        # The original combat system from DnD5 is not by itself
        # suitable here (because the player is alone against
        # several big monsters); for that reasons, the stats of
        # the player are secretly tuned here:
        aa = int( user.armor["cost"].bit_length() ) if user.armor else 6
        w.ac += aa + (user.level >>2) + 2
        w.hp += 2*user.level + (aa>>1) + 2
        dw = int( user.weapon["cost"].bit_length() ) if user.weapon else 1
        w.attacks[0]["attack_bonus"] += dw
        w.attacks[0]["damage_bonus"] += dw
        return w


    @staticmethod
    def battle(warriors):
        """
        Compute the battle between warriors and tell
        if focus_user (wich must be a warrior in warriors)
        is the winner or not.
        """

        # TODO: add counter for avoiding infinite battle?
        while len(warriors) > 1:
            # compute initiative
            for w in warriors: w.init_dice = randrange(1, 21) + w.dext_mod
            warriors.sort(key = lambda w: w.init_dice, reverse=True)
            #compute attacks
            for w in warriors:
                if w.attacks and w.hp > 0: # at least one attack and alive!
                    target, mymin = None, float("inf")
                    for m in warriors:
                        if m != w and m.hp > 0:
                            s = w.strategy(m)
                            if s < mymin: mymin, target = s, m
                    if target == None: continue # probably useless (TODO)
                    # resolve attack
                    dice, att = randrange(1, 21), choice(w.attacks)
                    if dice == 1: dice = float("-inf")
                    elif dice == 20: dice = float("inf")

                    if dice + att["attack_bonus"] >= target.ac:
                        # compute damage
                        target.hp -= att["damage_dice"]() + att["damage_bonus"]
                        # if dice > 20 (critical hit)
                        if dice > 20: target.hp -= att["damage_dice"]()
            # compute alive warriors
            warriors = [ w for w in warriors if w.hp > 0 ]
        return warriors[0]


    def fight(self, quest):
        """
        The user accepts a quest; fight is computed.
        Result is True(victory) False(lost)
        """
        me = self.build_warrior(self)
        warriors = [
            self.Warrior(m["hit_func"](), m["attacks"],
                    m["dexterity"], m["armor_class"], m["challenge_rating"],
                    (lambda m: int(m != me)))
                for m in quest.quest ]
        warriors.append(me)

        victory = me == self.battle(warriors)

        if victory:
            self.quest_win += 1
            xp_gain = sum(get_xp(m) for m in quest.quest)
            self.money += xp_gain
            self.give_xp(xp_gain)
        else:
            self.quest_lost += 1


        # update weapon count
        self.compute_proficiency()


        # compute idle time
        idle_base = 8 * 60 + 3 * (self.level ** 2) # between 8 and 28 min.
        # tiny reward for difficult challenges if victory
        idle_base -= victory * ( 4 * quest.total_cr )
        # Penalty if fight has been lost
        idle_base += ( not victory ) * ( 120 + 10 * (self.level - 1) )

        # min = 5min., max = 45min.
        idle_base = max(idle_base, 5*60)
        idle_base = min(idle_base, 45*60)

        self.idle_clock = time() + idle_base

        return victory


    def buy(self, item):
        # Everything has already been tested (availability + cost)
        if item["equipment_category"] == "Weapon":
            if self.weapon == item: return False
            self.weapon = item
            self.weapon_count = 0
            self.proficiency_bonus = 0
        else:
            if self.armor == item: return False
            self.armor = item
            ac = item["armor_class"]
            bonus = (self.dexterity-10)//2 if ac["dex_bonus"] else 0
            if ac["max_bonus"] != None: bonus = min(bonus, ac["max_bonus"])
            self.ac = ac["base"] + bonus
        self.money -= item["cost"]
        return True

    def set_strategy(self, strategy):
        strategy = strategy.lower()
        if strategy == "random":
            self.strategy = lambda m: random()
        elif strategy == "harsh": # weakest first (HP)
            self.strategy = lambda m: m.hp
        elif strategy == "rage": # less difficult first (CR)
            self.strategy = lambda m: m.cr
        elif strategy == "wise": # most difficult first (CR)
            self.strategy = lambda m: -m.cr
        else: return None
        return strategy


    def aboutme(self):
        """
        Return a message as a string
        """
        d = self.idle_clock - time()
        return (
              "Your stats: level " + str(self.level) + ", "
            + str(self.xp) + " XP, money = " + str(self.money)
            + ", current HP is " + str(self.hp) + "."
            + (( " You wield a " + self.weapon["name"]
                 + " (cost: " + str(self.weapon["cost"]) + ").")
                if self.weapon != None else "")
            + (( " You wear a " + self.armor["name"]
                 + " (cost: " + str(self.armor["cost"]) + ") .")
                if self.armor != None else "")
            + ((" You must wait " +  self.game.time_display(d) + ".")
                if d > 0 else "")
        )

def get_xp(m):
    cr = m["challenge_rating"]
    if cr == 0 and m["attacks"]: return 10
    elif cr < 1: xp = int(cr * 200)
    else:
        xp = ([ 200, 450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200,
            8400, 10000, 11500, 13000, 15000, 18000, 20000, 22000, 25000,
            33000, 41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000,
            155000 ])[cr-1]
    # The XP would increase too fast; ad-hoc correction below:
    return xp // (2*(int(cr)+1).bit_length())
