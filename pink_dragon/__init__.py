# -*- coding: utf-8 -*-

from .game import Game


"""
Legend of the Pink Dragon (game)

The game must be initialized with a "network" instance.


Eg. :

    >>> from game import Game
    >>> from game.networks import DebugNetwork
    >>> G = Game(DebugNetwork())


"""


__all__ = ['Game']
