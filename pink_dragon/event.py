# -*- coding: utf-8 -*-

from collections import defaultdict
from random import randrange, choice, sample
from .quest import Quest
from .user import get_xp
from time import time

class Event:
    """
    Event manager for the game.
    Called every two ticks of the internal Game._clock mechanism
    (called every 22min30s)
    """
    def __init__(self, game):
        self.game = game
        self.network = game.network
        self.ticks = 0
        self.data = defaultdict(int)

    def new_user(self, user):
        user.events = defaultdict(int)

    def get_tiers(self):
        """
        Return four list of users:
          * levels 1-4
          * levels 5-10
          * levels 11-16
          * levels 17-20
        """
        t = [[], [], [], []]
        for u in self.game.users.values():
            n = u.level
            t[ (n-1)//5 + int(n==5) - int(n==16) ].append(u)
        return t

    def get_sample_players(self, nbr, myfilter=lambda x: True):
        """
        Return a sample of 'nbr' players (optional filter).
        Return None if not enough users.
        """
        users = [ u for u in self.game.users.values()
                  if myfilter(u) ]
        if len(users) >= nbr:
            return sample(users, nbr)


    def random_users_for_event(self, nbr, event_type, delta_ticks):
        """
        Return a sample of 'nbr' users such that their clock for
        the event_type is at least delta_ticks.
        One tick is 22min30s (3 hours are 8 ticks).
        Clock for this even_type is adjusted for the selected users.
        Can return None
        """
        u = self.get_sample_players(nbr, myfilter =
                lambda e: self.ticks - e.events[event_type] >= delta_ticks)
        if u:
            for e in u: e.events[event_type] = self.ticks
            return u

    def battle(self, nbr):
        """
        Pick nbr random players, and compute the winner
        """
        tiers = [ t for t in self.get_tiers() if len(t) >= nbr ]
        if not tiers: return
        players = sample(choice(tiers), nbr)
        warriors = []
        msg = self.network.header_event + choice([
          " Battle of the Desperate Souls: ",
          " Battle for the Forgotten Realm: ",
          " Battle for the Forbidden Glory: ",
          " Battle for the Salvation of the Rusty Crown: ",
          " Battle of the Ultimate Hope: ",
          " Battle of the Fallen Honor: ",
          " Battle of the Immortal Horror: ",
          " Battle of the Infinite Despair: ",
          " Battle of the Legendary Fields: "
          ])
        for p in players:
            w = p.build_warrior(p, strict=True)
            w.owner = p
            warriors.append(w)
            msg = msg + ( self.network.code_bold + p.nick
                + self.network.code_reset
                + " (level " + str(p.level) + "), " )
        winner = players[0].battle(warriors).owner
        self.network.broadcast(msg[:-2] + ".")
        self.network.broadcast(self.network.header_event
                + " Winner of the current battle is "
                + self.network.code_bold
                + winner.nick + self.network.code_reset + "!" )
        xp_gain = sum( p.xp for p in players) - winner.xp
        xp_gain = (( xp_gain // xp_gain.bit_length())
                       if xp_gain else 5)
        winner.give_xp(xp_gain)
        # special reward with probability 1/3
        if randrange(0, 3) == 0:
            winner.idle_clock = 0
            self.network.broadcast(self.network.header_event
                    + " As a special reward, "
                    + self.network.code_bold
                    + winner.nick + self.network.code_reset
                    +"'s idle time is set to 0.")



    def run(self, force=None):
        """
        Main method called every 22min30s
        """
        self.ticks += 1
        r = force if force != None else randrange(0, 26)
        if r == 0: # replace a quest
            self.game.bboard.replace_quest(1, display=True)
        elif r == 1: # add a quest
            self.network.broadcast(
                    "BREAKING NEWS: More and more creeps are prowling each day in the Realm!")
            self.game.bboard.add_new_quest(display=True)
        elif r == 2: # complete quests
            self.game.bboard.complete()
        elif r == 3: # remove a quests
            if len(self.game.bboard.quests) > 8:
                self.game.bboard.remove_quest(
                    self.game.bboard.select_quest_to_remove())
        elif r == 4: self.battle(2) # 2 players are fighting
        elif r == 5: self.battle(3) # 3 players are fighting
        elif r == 6: self.battle(4) # 4 players are fighting
        elif r == 7: self.battle(5) # 5 players are fighting
        elif r == 8: self.battle(randrange(6, 13))
        elif r == 9:
            user = self.random_users_for_event(1, "chance", 24)
            if user == None: return
            user = user[0]
            money = (user.level + randrange(0,3))**2 * 15
            user.money += money
            self.network.broadcast( self.network.header_event
                + " " + self.network.code_bold
                + user.nick + self.network.code_reset
                + " was rummaging in the bush and just found "
                + str(money) + " coins! (Current money: "
                + str(user.money) + ")")
        elif r == 10:
            if self.ticks - self.data["dragon_seen"] >= 8:
                self.data["dragon_seen"] = self.ticks
                self.network.broadcast(
                    "BREAKING NEWS: The Pink Dragon was seen " +
                     choice([
                       "at the north of the Realm.",
                       "at the south of the Realm.",
                       "in the eastern Mountains.",
                       "in the western Forests.",
                       "devouring cattle in the Fields.",
                       "hovering above the Castle."
                    ]) + " (Game running for "
                     + self.game.time_display(
                          time() - self.game.start_time)
                     + ", with " + str(len(self.game.users))
                     + " adventurers now in the Realm)"
                     )
        elif r == 11 or r == 12: # top eight scores
            if self.ticks - self.data["display_scores"] >= 4:
                u = list(self.game.users.values())
                if u: self.data["display_scores"] = self.ticks
                u.sort(key = lambda e: e.xp, reverse = True)
                for rank, e in zip(range(1, 9), u):
                    self.network.broadcast("[Top Adventurers] "
                        + self.network.code_bold + e.nick
                        + self.network.code_reset + ", level "
                        + str(e.level) + ", has "
                        + str(e.xp) + " XP (rank " + str(rank) +")")
        elif r == 13: # potion of gain/lose XP
            user = self.random_users_for_event(1, "potion", 16)
            if user == None: return
            user = user[0]
            blessed = randrange(0, 2)
            base_xp = max(10, user.xp // 10)
            if blessed:
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + user.nick
                        + self.network.code_reset
                        + " has found a blessed potion of"
                        + " gain experience!" )
                user.give_xp(base_xp)
            else: # cursed
                base_xp >>= 1
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + user.nick
                        + self.network.code_reset
                        + " has found a cursed potion of"
                        + " gain experience! " + user.nick
                        + " has lost " + str(base_xp) + " XP!")
                user.xp = max(0, user.xp - base_xp)
        elif r == 14: # potion of speed / paralysis
            # cursed or not
            user = self.random_users_for_event(1, "potion", 16)
            if user == None: return
            user = user[0]
            blessed = randrange(0, 2)
            if blessed:
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + user.nick
                        + self.network.code_reset
                        + " has found a blessed potion of speed! "
                        + user.nick + "'s idle time is set to 0." )
                user.idle_clock = 0
            else: # cursed
                i = 60 * (5 + user.level)
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + user.nick
                        + self.network.code_reset
                        + " has found a cursed potion of"
                        + " paralysis! " + str(i)
                        + " seconds have been added to "
                        + user.nick
                        + "'s idle time!")
                user.idle_clock = max(user.idle_clock + i, time() + i)
        elif r == 15: # potion of gain stats (strength, dexterity, hp)
            # cursed or not
            user = self.random_users_for_event(1, "potion", 16)
            if user == None: return
            user = user[0]
            ability = randrange(0, 3)
            if ability == 0: # strength
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + user.nick
                        + self.network.code_reset
                        + " has found a blessed potion of strength!"
                        + " (+1 STR)" )
                user.strength += 1
            elif ability == 1: # dexterity
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + user.nick
                        + self.network.code_reset
                        + " has found a blessed potion of dexterity!"
                        + " (+1 DEX)" )
                user.dexterity += 1
            else: # HP
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + user.nick
                        + self.network.code_reset
                        + " has found a blessed potion of endurance!"
                        + " (+1 HP)" )
                user.hp += 1
        elif r == 16: # Path of the Glory
            user = self.random_users_for_event(1, "glory", 256) # 4 days
            if user == None: return
            user = user[0]
            Q = [ Quest() for _ in range(20) ]
            Q.sort(key=lambda q: q.total_cr)
            Q = Q[8:] # discard eight initial quests
            # Keep the initial idle_clock for restoring it
            keep_idle_clock = user.idle_clock
            for stage, quest in enumerate(Q, start=1):
                self.network.broadcast( self.network.header_event
                    + " " + self.network.code_bold
                    + user.nick + self.network.code_reset
                    + " (level " + str(user.level) + ")"
                    + " follows the Path of the Glory. Stage "
                    + str(stage) + ": " + quest.display )
                if user.fight(quest):
                    xp_gain = sum(get_xp(m) for m in quest.quest)
                else:
                    self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold
                        + user.nick + self.network.code_reset
                        + " was defeated during stage "
                        + str(stage) + " of the Path of Glory!" )
                    user.idle_clock = keep_idle_clock
                    break
            else: # user has completed all (12) quests!
                self.network.broadcast( self.network.header_event
                    + " " + self.network.code_bold
                    + user.nick + self.network.code_reset
                    + " has completed all " + str(len(Q))
                    + " quests on the Path of Glory!" )
                self.network.broadcast(self.network.header_event
                        + " The King gives 15,000 coins to "
                        + self.network.code_bold
                        + user.nick + self.network.code_reset
                        +".")
                self.network.broadcast(self.network.header_event
                        + " As a special reward, "
                        + self.network.code_bold
                        + user.nick + self.network.code_reset
                        +"'s idle time is set to 0.")
                user.money += 15000
                user.idle_clock = 0
        elif r == 17 or r == 18 or r == 19: # Epic quest
            user = self.random_users_for_event(3, "quest", 32) # 12 hours
            if user == None: return
            Q = Quest.build_quest(32, 70)
            msg = ( self.network.header_event + " Epic Quest: " )
            warriors = []
            for p in user:
                w = p.build_warrior(p, strict=False)
                w.owner = p
                w.strategy = ( lambda m:
                    999999 if hasattr(m, "owner") # not float("inf")!!!
                    else p.strategy(m) )
                warriors.append(w)
                msg = msg + ( self.network.code_bold + p.nick
                    + self.network.code_reset
                    + " (level " + str(p.level) + "), " )
            _warriors_ = list(warriors)
            self.network.broadcast(msg[:-2] + " are fighting against: "
                    + Q.display)
            for m in Q.quest:
                warriors.append( user[0].Warrior(
                    m["hit_func"](), m["attacks"],
                    m["dexterity"], m["armor_class"], m["challenge_rating"],
                    (lambda m: int(m != choice(_warriors_)))))
            result = user[0].battle(warriors)
            if hasattr(result, "owner"):
                xp_gain = sum(get_xp(m) for m in Q.quest) // 3
                self.network.broadcast(
                        self.network.header_event + " Epic Quest: "
                        + "Adventurers defeated all monsters!" )
                for u in user: u.give_xp(xp_gain)
            else:
                self.network.broadcast(
                        self.network.header_event + " Epic Quest: "
                        + "Adventurers are defeated!" )
        elif r == 20 or r == 21: # Creepy quest
            user = self.random_users_for_event(4, "quest", 32) # 12 hours
            if user == None: return
            Q = Quest.build_quest(32, 45, epic=False)
            msg = ( self.network.header_event + " Creepy Quest: " )
            warriors = []
            for p in user:
                w = p.build_warrior(p, strict=False)
                w.owner = p
                w.strategy = ( lambda m:
                    999999 if hasattr(m, "owner") # not float("inf")!!!
                    else p.strategy(m) )
                warriors.append(w)
                msg = msg + ( self.network.code_bold + p.nick
                    + self.network.code_reset
                    + " (level " + str(p.level) + "), " )
            _warriors_ = list(warriors)
            self.network.broadcast(msg[:-2] + " are fighting against: "
                    + Q.display)
            for m in Q.quest:
                warriors.append( user[0].Warrior(
                    m["hit_func"](), m["attacks"],
                    m["dexterity"], m["armor_class"], m["challenge_rating"],
                    (lambda m: int(m != choice(_warriors_)))))
            result = user[0].battle(warriors)
            if hasattr(result, "owner"):
                xp_gain = sum(get_xp(m) for m in Q.quest) // 3
                self.network.broadcast(
                        self.network.header_event + " Creepy Quest: "
                        + "Adventurers defeated all creeps!" )
                for u in user: u.give_xp(xp_gain)
            else:
                self.network.broadcast(
                        self.network.header_event + " Creepy Quest: "
                        + "Adventurers are defeated!" )
        elif r == 22: # Dexterity check (by tier)
            tiers = self.get_tiers()
            for n, t in enumerate(tiers):
                if t:
                    msg = self.network.header_event + " Dexterity check: "
                    msg = msg + ([ "first tier (levels 1-4)",
                            "second tier (levels 5-10)",
                            "third tier (levels 11-16)",
                            "fourth tier (levels 17-20)" ])[n] + ":"
                    round = 0
                    if len(t) == 1: msg = msg + " Single player."
                    while len(t) > 1:
                        round += 1
                        msg = msg + " round " + str(round)
                        msg = msg + " (" + str(len(t)) + " players),"
                        p = [ (randrange(1,21)
                                  + min(5, (u.dexterity - 10)//2), u)
                              for u in t ]
                        m = max(p, key=lambda e: e[0])[0]
                        t = [ e[1] for e in p if e[0]==m ]
                    w = t[0]
                    msg = ( msg[:-1] + ". Winner is " + 
                             self.network.code_bold + w.nick
                            + self.network.code_reset
                            + " (level " + str(w.level) + ")." )
                    xp_gain = (w.xp // w.xp.bit_length()) if w.xp else 5
                    coins = max(5, (xp_gain//100)*100)
                    msg = ( msg + " " + w.nick + " gets "
                            + str(xp_gain) + " XP and "
                            + str(coins) + " coins." )
                    self.network.broadcast(msg)
                    w.money += coins
                    w.give_xp(xp_gain)
                    if n == 3:
                        self.network.broadcast( w.nick
                                + " is the Dude!" )
        elif r == 23: # Strength check (by tier)
            tiers = self.get_tiers()
            for n, t in enumerate(tiers):
                if t:
                    msg = self.network.header_event + " Strength check: "
                    msg = msg + ([ "first tier (levels 1-4)",
                            "second tier (levels 5-10)",
                            "third tier (levels 11-16)",
                            "fourth tier (levels 17-20)" ])[n] + ":"
                    round = 0
                    if len(t) == 1: msg = msg + " Single player."
                    while len(t) > 1:
                        round += 1
                        msg = msg + " round " + str(round)
                        msg = msg + " (" + str(len(t)) + " players),"
                        p = [ (randrange(1,21)
                                  + min(5, (u.strength - 10)//2), u)
                              for u in t ]
                        m = max(p, key=lambda e: e[0])[0]
                        t = [ e[1] for e in p if e[0]==m ]
                    w = t[0]
                    msg = ( msg[:-1] + ". Winner is " + 
                             self.network.code_bold + w.nick
                            + self.network.code_reset
                            + " (level " + str(w.level) + ")." )
                    xp_gain = (w.xp // w.xp.bit_length()) if w.xp else 5
                    coins = max(5, (xp_gain//100)*100)
                    msg = ( msg + " " + w.nick + " gets "
                            + str(xp_gain) + " XP and "
                            + str(coins) + " coins." )
                    self.network.broadcast(msg)
                    w.money += coins
                    w.give_xp(xp_gain)
                    if n == 3:
                        self.network.broadcast( w.nick
                                + " is the Boss!" )
        elif r == 24: # Steal gold
            victim = self.random_users_for_event(1, "victim", 64)
            if not victim or victim[0].money < 10: return
            victim = victim[0]
            thief = self.random_users_for_event(1, "chance", 32)
            if not thief: return
            thief = thief[0]
            money = (( victim.money // victim.money.bit_length())
                          if victim.money else 0)
            if not money: return
            # Dexterity check (1: nothing, 20: full)
            dc = randrange(1, 21) + min(5, (u.dexterity - 10)//2)
            money = ((dc+4) * money) // 29
            if money:
                victim.money -= money
                thief.money += money
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + thief.nick
                        + self.network.code_reset + " has stolen "
                        + str(money) + " coins from "
                        + " " + self.network.code_bold + victim.nick
                        + self.network.code_reset + "!" )
            else: # attempt failed
                i = 15 * 60 
                thief.idle_clock = max(thief.idle_clock + i, time() + i)
                self.network.broadcast( self.network.header_event
                        + " " + self.network.code_bold + thief.nick
                        + self.network.code_reset + " tried stealing "
                        + " money from " + self.network.code_bold + victim.nick
                        + self.network.code_reset
                        + " but failed! " + thief.nick
                        + " will have to wait 15 minutes more before next turn!" )
        elif r == 25: # Merge two quests (if at least 10 quests on BBoard)
            if len(self.game.bboard.quests) > 9:
                Q = sorted(self.game.bboard.quests.items(),
                        key=lambda q: q[1].total_cr)
                n = randrange(0, len(Q)-1)
                a, b = choice([ (Q[n], Q[n+1]), (Q[n+1], Q[n]) ])
                a[1].quest.extend(b[1].quest)
                a[1].creation_time = self.game.tick_turn
                a[1].adjust()
                del self.game.bboard.quests[b[0]]
                self.network.broadcast( self.network.header_event
                        + ( " Monsters from quest %s have decided to move "
                            + "into quest %s; both quests have been merged." )
                        % (b[0], a[0])
                        + " Quest %s is removed from the Bulletin Board."
                        % (b[0],))
                self.game.bboard.broadcast_quest(a[0])
