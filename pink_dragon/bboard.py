# -*- coding: utf-8 -*-

from .quest import Quest
from random import randrange, choice, random
from time import time

class BBoard:
    def __init__(self, game):
        self.game = game
        self.network = game.network
        self.tick_cnt = 0
        self.quest_cnt = -1
        self.quests = dict()
        for i in range(16): # Initially create 16 (easy) quests
            self.add_new_quest(display=False, level=randrange(0,4))
        self.display_quests()

    def tick(self): # one tick every 45 minutes
        self.tick_cnt = (self.tick_cnt + 1) & 31 # modulo 32, restart every 24h
        if self.tick_cnt & 1:
            self.replace_quest( 1 + (self.tick_cnt == 17) )
        self.display_quests()

    def broadcast_quest(self, myid):
        self.network.broadcast(
            self.network.header_bboard
            + " Quest " + myid + ": "
            + self.quests[myid].display )

    def display_quests(self):
        L = [ ( q.total_cr, myid ) for myid, q in self.quests.items() ]
        L.sort()
        self.network.broadcast( self.network.header_bboard
                + " Currently available quests:")
        for _, myid in L:
            self.broadcast_quest(myid)

    def remove_quest(self, myid):
        self.network.broadcast(
            (choice([
                "Monsters from Quest %s are getting too old for fighting again; they are now retiring.",
                "Monsters from Quest %s feel faint this morning.",
                "Monsters from Quest %s are on vacation for the week.",
                "Monsters from Quest %s are too tired for fighting.",
                "Monsters from Quest %s have left the Realm!",
                "BREAKING NEWS: monsters from Quest %s go on strike!",
                "Monsters from Quest %s have been devoured by monsters from another quest.",
                "Monsters from Quest %s have decided to turn into nice pets for enjoying children of the Realm.",
                "Monsters from Quest %s are getting depressed.",
                "The King has pardoned monsters from Quest %s."
            ]) % (myid,))
            + " Quest is removed from the Bulletin Board.")
        del self.quests[myid]

    def add_new_quest(self, display=False, level=None):
        """
        Add a new quest to the BBoard; it is chosen randomly unless
        a "level" is provided (0 <= level < 10); see quest.py
        """
        self.quest_cnt = (self.quest_cnt + 1) & 65535
        myid, q = ("%04X" % (self.quest_cnt,)), Quest(force=level)
        q.creation_time = self.game.tick_turn
        self.quests[myid] = q
        if display: self.broadcast_quest(myid)
        return myid

    def select_quest_to_remove(self):
        return (
            sorted(self.quests.items(), key=lambda q: q[1].creation_time)
            )[ int((random()**2) * (len(self.quests) - 0.5)) ][0]

    def replace_quest(self, nbr, display=False):
        for _ in range(nbr):
            self.remove_quest( self.select_quest_to_remove() )
        for _ in range(nbr):
            self.add_new_quest(display=display)

    def accept_quest(self, user, myid):
        """
        Return True if user has ascended
        """
        myid = myid.upper()
        t = time()
        if myid in self.quests:
            if t > user.idle_clock:
                q = self.quests[myid]
                self.network.broadcast( 
                        self.network.header_quest + " "
                        + self.network.code_bold
                        + user.nick + self.network.code_reset
                        + " has accepted quest " + myid
                        + " (total CR = " + str(q.total_cr)
                        + "): " + q.display + ".")
                result = user.fight(q)
                self.network.broadcast(
                        self.network.header_quest + " "
                        +  self.network.code_bold
                        + user.nick + self.network.code_reset
                    + (([" has lost! Quest %s is still available.",
                        " has won! Quest %s has been removed from the Bulletin Board."])[result] % (myid,)) )
                if result:
                    if q.total_cr >= 65: # Ascension
                        self.network.broadcast(
                           self.network.code_bold + "** New Ascension **"
                           + self.network.code_reset )
                        self.network.broadcast(
                           self.network.code_bold + "** " + user.nick
                           + " (level " + str(user.level)
                           + ") has completed the game in "
                           + self.game.time_display(t - user.creation_time)
                           + "! **" + self.network.code_reset )
                        self.network.broadcast(
                           self.network.code_bold + user.nick
                           + self.network.code_reset
                           + " has now left the Realm with "
                           + str(user.xp) + " XP.")
                        self.network.broadcast(
                           self.network.code_bold + user.nick
                           + self.network.code_reset
                           + " is allowed to reincarnate and start again!")
                        user.idle_clock = 0
                        del self.game.users[user.nick]
                        return True
                    del self.quests[myid]
                    self.complete()
        else:
            self.network.reply("The quest " + myid + " is currently unavailable!")
        # Always display idle time (even if quest was unavailable)
        if t < user.idle_clock:
            self.network.reply("You must rest for "
                + self.game.time_display(user.idle_clock - t) + "!")
        return False

    def complete(self):
        n = len(self.quests)
        nbr_min, nbr_max = 0, 2
        if n < 8: nbr_min = 1
        if n < 10: nbr_max = 3
        if n > 16: nbr_max = 1 # if n == 16, 2 are allowed --> 18 max quests
        for _ in range(randrange(nbr_min, nbr_max + 1)):
            self.add_new_quest(display=True)
