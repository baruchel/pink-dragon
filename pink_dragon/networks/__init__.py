# -*- coding: utf-8 -*-

from .debug_network import DebugNetwork
from .irc_network import IrcNetwork

__all__ = ['DebugNetwork', 'IrcNetwork']
