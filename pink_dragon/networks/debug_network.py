# -*- coding: utf-8 -*-

from time import time, sleep
from threading import Thread

class DebugNetwork:
    def __init__(self):
        self.code_red = "\x1b[31m"
        self.code_green = "\x1b[32m"
        self.code_yellow = "\x1b[33m"
        self.code_blue = "\x1b[34m"
        self.code_magenta = "\x1b[35m"
        # common attributes to be implemented
        self.code_bold = "\x1b[1m"
        self.code_reset = "\x1b[0m"
        self.code_bboard = self.code_magenta
        self.code_shop = self.code_green
        self.code_event = self.code_yellow
        self.code_quest = self.code_red
        self.header_bboard = (
                 self.code_bold + "[" + self.code_reset
               + self.code_bboard + "BBOARD" + self.code_reset
               + self.code_bold + "]" + self.code_reset )
        self.header_shop = (
                 self.code_bold + "[" + self.code_reset
               + self.code_shop + "STORE" + self.code_reset
               + self.code_bold + "]" + self.code_reset )
        self.header_event = (
                 self.code_bold + "[" + self.code_reset
               + self.code_event + "EVENT" + self.code_reset
               + self.code_bold + "]" + self.code_reset )
        self.header_quest = (
                 self.code_bold + "[" + self.code_reset
               + self.code_quest + "QUEST" + self.code_reset
               + self.code_bold + "]" + self.code_reset )

    def broadcast(self, msg):
        print(msg)


    def reply(self, msg):
        print("[reply] " + msg)

    def addEvent(self, callback, delay):
        # this is a test 'network'; delay is divided by 15 in order to
        # have all events happen much quicker
        def C(d):
            sleep(d/15)
            callback()
        process = Thread(target=C, args=[delay])
        process.start()
