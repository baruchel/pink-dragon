# -*- coding: utf-8 -*-

from time import time, sleep

class IrcNetwork:
    def __init__(self, broadcast_func, reply_func, addEvent_func):
        self.broadcast_func = broadcast_func
        self.reply_func = reply_func
        self.addEvent_func = addEvent_func
        self.code_red = "\x0304"
        self.code_green = "\x0303"
        self.code_yellow = "\x0307"
        self.code_blue = "\x0302"
        self.code_magenta = "\x0306"
        # common attributes to be implemented
        self.code_bold = "\x02"
        self.code_reset = "\x0f"
        self.code_bboard = self.code_magenta
        self.code_shop = self.code_green
        self.code_event = self.code_yellow
        self.code_quest = self.code_red
        self.header_bboard = (
                 self.code_bold + "[" + self.code_reset
               + self.code_bboard + "BBOARD" + self.code_reset
               + self.code_bold + "]" + self.code_reset )
        self.header_shop = (
                 self.code_bold + "[" + self.code_reset
               + self.code_shop + "STORE" + self.code_reset
               + self.code_bold + "]" + self.code_reset )
        self.header_event = (
                 self.code_bold + "[" + self.code_reset
               + self.code_event + "EVENT" + self.code_reset
               + self.code_bold + "]" + self.code_reset )
        self.header_quest = (
                 self.code_bold + "[" + self.code_reset
               + self.code_quest + "QUEST" + self.code_reset
               + self.code_bold + "]" + self.code_reset )

    def broadcast(self, msg):
        self.broadcast_func(msg)


    def reply(self, msg):
        self.reply_func(msg)

    def addEvent(self, callback, delay):
        self.addEvent_func(callback, delay)
