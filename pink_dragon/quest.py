# -*- coding: utf-8 -*-

from .monsters import monster_list
import collections
from random import randrange, random, choice
from functools import reduce
from itertools import groupby

# monster_catalogue = { m["name"]:m for m in monster_list }

monster_type = collections.defaultdict(list)
monster_align = collections.defaultdict(list)
monster_size = collections.defaultdict(list)
monster_cr = collections.defaultdict(list)

for m in monster_list:
    monster_type[m["type"]].append(m)
    monster_align[m["alignment"]].append(m)
    monster_size[m["size"]].append(m)
    monster_cr[m["challenge_rating"]].append(m)

monster_type = dict(monster_type)
monster_align = dict(monster_align)
monster_size = dict(monster_size)
monster_cr = dict(monster_cr)


# [('undead', 19), ('aberration', 5), ('swarm of Tiny beasts', 10), ('ooze', 4), ('plant', 6), ('dragon', 43), ('giant', 10), ('construct', 9), ('fey', 6), ('beast', 88), ('celestial', 6), ('fiend', 23), ('monstrosity', 40), ('humanoid', 40), ('elemental', 16)]


# Pools of monsters by CR
# [(0, 29), (0.125, 19), (0.25, 32), (0.5, 33), (1, 25), (2, 43), (3, 20), (4, 11), (5, 25), (6, 10), (7, 6), (8, 9), (9, 7), (10, 6), (11, 8), (12, 3), (13, 6), (14, 3), (15, 4), (16, 5), (17, 5), (19, 1), (20, 3), (21, 4), (22, 2), (23, 3), (24, 2), (30, 1)]
pool_easy = reduce(list.__add__,
        [ monster_cr[k] for k in monster_cr if k <= 1 ])
pool_medium1 = reduce(list.__add__,
        [ monster_cr[k] for k in monster_cr if 1 < k <= 10 ])
pool_medium2 = reduce(list.__add__,
        [ monster_cr[k] for k in monster_cr if 5 < k <= 16 ])
pool_difficult = reduce(list.__add__,
        [ monster_cr[k] for k in monster_cr if 13 < k ])

def pickup_random(L):
    """
    choose a monster from the pool L, then return it with
    a copy of the list containing compliant monsters (alignment)
    """
    m = choice(L)
    # particular case of the "Cloud Giant" (neutral good (50%) or neutral evil (50%))
    if m["name"] == "Cloud Giant":
        m = dict(m)
        m["alignment"] = (["neutral good", "neutral evil"])[randrange(0,2)]
    L2, a, s = [], m["alignment"], m["size"]
    for m2 in L:
        if m["name"] == m2["name"]: continue
        A = { a, m2["alignment"] }
        if A == {'any non-good alignment'}: valid = True
        elif A == {'any non-good alignment', 'neutral'}: valid = True
        elif A == {'any non-good alignment', 'lawful good'}: valid = False
        elif A == {'any non-good alignment', 'any evil alignment'}: valid = True
        elif A == {'any non-good alignment', 'chaotic neutral'}: valid = True
        elif A == {'unaligned', 'any non-good alignment'}: valid = True
        elif A == {'lawful neutral', 'any non-good alignment'}: valid = True
        elif A == {'any non-good alignment', 'any alignment'}: valid = True
        elif A == {'any non-good alignment', 'neutral evil'}: valid = True
        elif A == {'any non-good alignment', 'any non-lawful alignment'}: valid = True
        elif A == {'any non-good alignment', 'lawful evil'}: valid = True
        elif A == {'any non-good alignment', 'chaotic good'}: valid = False
        elif A == {'any non-good alignment', 'neutral good'}: valid = False
        elif A == {'chaotic evil', 'any non-good alignment'}: valid = True
        elif A == {'any non-good alignment', 'any chaotic alignment'}: valid = True
        elif A == {'neutral'}: valid = True
        elif A == {'neutral', 'lawful good'}: valid = False
        elif A == {'neutral', 'any evil alignment'}: valid = True
        elif A == {'neutral', 'chaotic neutral'}: valid = True
        elif A == {'unaligned', 'neutral'}: valid = True
        elif A == {'lawful neutral', 'neutral'}: valid = False
        elif A == {'neutral', 'any alignment'}: valid = True
        elif A == {'neutral', 'neutral evil'}: valid = False
        elif A == {'any non-lawful alignment', 'neutral'}: valid = True
        elif A == {'lawful evil', 'neutral'}: valid = False
        elif A == {'neutral', 'chaotic good'}: valid = False
        elif A == {'neutral', 'neutral good'}: valid = False
        elif A == {'chaotic evil', 'neutral'}: valid = False
        elif A == {'neutral', 'any chaotic alignment'}: valid = True
        elif A == {'lawful good'}: valid = True
        elif A == {'lawful good', 'any evil alignment'}: valid = False
        elif A == {'chaotic neutral', 'lawful good'}: valid = False
        elif A == {'unaligned', 'lawful good'}: valid = True
        elif A == {'lawful neutral', 'lawful good'}: valid = False
        elif A == {'any alignment', 'lawful good'}: valid = True
        elif A == {'lawful good', 'neutral evil'}: valid = False
        elif A == {'any non-lawful alignment', 'lawful good'}: valid = False
        elif A == {'lawful evil', 'lawful good'}: valid = False
        elif A == {'chaotic good', 'lawful good'}: valid = False
        elif A == {'neutral good', 'lawful good'}: valid = False
        elif A == {'chaotic evil', 'lawful good'}: valid = False
        elif A == {'lawful good', 'any chaotic alignment'}: valid = False
        elif A == {'any evil alignment'}: valid = True
        elif A == {'chaotic neutral', 'any evil alignment'}: valid = False
        elif A == {'unaligned', 'any evil alignment'}: valid = True
        elif A == {'lawful neutral', 'any evil alignment'}: valid = False
        elif A == {'any alignment', 'any evil alignment'}: valid = True
        elif A == {'neutral evil', 'any evil alignment'}: valid = True
        elif A == {'any non-lawful alignment', 'any evil alignment'}: valid = True
        elif A == {'lawful evil', 'any evil alignment'}: valid = True
        elif A == {'chaotic good', 'any evil alignment'}: valid = False
        elif A == {'neutral good', 'any evil alignment'}: valid = False
        elif A == {'chaotic evil', 'any evil alignment'}: valid = True
        elif A == {'any evil alignment', 'any chaotic alignment'}: valid = True
        elif A == {'chaotic neutral'}: valid = True
        elif A == {'unaligned', 'chaotic neutral'}: valid = True
        elif A == {'lawful neutral', 'chaotic neutral'}: valid = False
        elif A == {'any alignment', 'chaotic neutral'}: valid = True
        elif A == {'chaotic neutral', 'neutral evil'}: valid = False
        elif A == {'any non-lawful alignment', 'chaotic neutral'}: valid = True
        elif A == {'lawful evil', 'chaotic neutral'}: valid = False
        elif A == {'chaotic good', 'chaotic neutral'}: valid = False
        elif A == {'neutral good', 'chaotic neutral'}: valid = False
        elif A == {'chaotic evil', 'chaotic neutral'}: valid = False
        elif A == {'chaotic neutral', 'any chaotic alignment'}: valid = True
        elif A == {'unaligned'}: valid = True
        elif A == {'lawful neutral', 'unaligned'}: valid = True
        elif A == {'unaligned', 'any alignment'}: valid = True
        elif A == {'unaligned', 'neutral evil'}: valid = True
        elif A == {'unaligned', 'any non-lawful alignment'}: valid = True
        elif A == {'unaligned', 'lawful evil'}: valid = True
        elif A == {'unaligned', 'chaotic good'}: valid = True
        elif A == {'unaligned', 'neutral good'}: valid = True
        elif A == {'chaotic evil', 'unaligned'}: valid = True
        elif A == {'unaligned', 'any chaotic alignment'}: valid = True
        elif A == {'lawful neutral'}: valid = True
        elif A == {'lawful neutral', 'any alignment'}: valid = True
        elif A == {'lawful neutral', 'neutral evil'}: valid = False
        elif A == {'lawful neutral', 'any non-lawful alignment'}: valid = False
        elif A == {'lawful neutral', 'lawful evil'}: valid = False
        elif A == {'lawful neutral', 'chaotic good'}: valid = False
        elif A == {'lawful neutral', 'neutral good'}: valid = False
        elif A == {'chaotic evil', 'lawful neutral'}: valid = False
        elif A == {'lawful neutral', 'any chaotic alignment'}: valid = False
        elif A == {'any alignment'}: valid = True
        elif A == {'any alignment', 'neutral evil'}: valid = True
        elif A == {'any non-lawful alignment', 'any alignment'}: valid = True
        elif A == {'lawful evil', 'any alignment'}: valid = True
        elif A == {'chaotic good', 'any alignment'}: valid = True
        elif A == {'neutral good', 'any alignment'}: valid = True
        elif A == {'chaotic evil', 'any alignment'}: valid = True
        elif A == {'any alignment', 'any chaotic alignment'}: valid = True
        elif A == {'neutral evil'}: valid = True
        elif A == {'any non-lawful alignment', 'neutral evil'}: valid = True
        elif A == {'lawful evil', 'neutral evil'}: valid = False
        elif A == {'chaotic good', 'neutral evil'}: valid = False
        elif A == {'neutral good', 'neutral evil'}: valid = False
        elif A == {'chaotic evil', 'neutral evil'}: valid = False
        elif A == {'neutral evil', 'any chaotic alignment'}: valid = False
        elif A == {'any non-lawful alignment'}: valid = True
        elif A == {'lawful evil', 'any non-lawful alignment'}: valid = False
        elif A == {'any non-lawful alignment', 'chaotic good'}: valid = True
        elif A == {'any non-lawful alignment', 'neutral good'}: valid = True
        elif A == {'chaotic evil', 'any non-lawful alignment'}: valid = True
        elif A == {'any non-lawful alignment', 'any chaotic alignment'}: valid = True
        elif A == {'lawful evil'}: valid = True
        elif A == {'lawful evil', 'chaotic good'}: valid = False
        elif A == {'lawful evil', 'neutral good'}: valid = False
        elif A == {'chaotic evil', 'lawful evil'}: valid = False
        elif A == {'lawful evil', 'any chaotic alignment'}: valid = False
        elif A == {'chaotic good'}: valid = True
        elif A == {'chaotic good', 'neutral good'}: valid = False
        elif A == {'chaotic evil', 'chaotic good'}: valid = False
        elif A == {'chaotic good', 'any chaotic alignment'}: valid = True
        elif A == {'neutral good'}: valid = True
        elif A == {'chaotic evil', 'neutral good'}: valid = False
        elif A == {'neutral good', 'any chaotic alignment'}: valid = False
        elif A == {'chaotic evil'}: valid = True
        elif A == {'chaotic evil', 'any chaotic alignment'}: valid = True
        elif A == {'any chaotic alignment'}: valid = True
        if not valid: continue
        S = { s, m2["size"] }
        if S == {'Large'}: valid = True
        elif S == {'Large', 'Gargantuan'}: valid = True
        elif S == {'Tiny', 'Large'}: valid = False
        elif S == {'Medium', 'Large'}: valid = True
        elif S == {'Huge', 'Large'}: valid = True
        elif S == {'Small', 'Large'}: valid = False
        elif S == {'Gargantuan'}: valid = True
        elif S == {'Tiny', 'Gargantuan'}: valid = False
        elif S == {'Medium', 'Gargantuan'}: valid = True
        elif S == {'Huge', 'Gargantuan'}: valid = True
        elif S == {'Small', 'Gargantuan'}: valid = False
        elif S == {'Tiny'}: valid = True
        elif S == {'Tiny', 'Medium'}: valid = True
        elif S == {'Tiny', 'Huge'}: valid = False
        elif S == {'Small', 'Tiny'}: valid = True
        elif S == {'Medium'}: valid = True
        elif S == {'Huge', 'Medium'}: valid = True
        elif S == {'Small', 'Medium'}: valid = True
        elif S == {'Huge'}: valid = True
        elif S == {'Small', 'Huge'}: valid = False
        elif S == {'Small'}: valid = True
        if not valid: continue
        L2.append(m2)
    return m, L2




class Quest:
    def __init__(self, force=None):
        # difficulty of the quest
        dif = randrange(0, 10) if force==None else force
        if dif < 3:   pool = pool_easy       # 30%
        elif dif < 5: pool = pool_medium1    # 20%
        elif dif < 7: pool = pool_medium2    # 20%
        else:         pool = pool_difficult  # 30%
        S = []
        # The variable dif can be 10 or more ONLY through the use of 'force'
        # keyword variable; in such case, the 'difficult' pool is selected
        # but external commands can use the following line for selecting
        # more monsters (for instance force=10 will adjust the number of monsters
        # as if dif=0)
        if dif >= 10: dif %= 10
        while pool and len(S) < 3:
            m, pool = pickup_random(pool)
            S.append(m)
            if len(S) == 1 and random() < .45: break
            if len(S) == 2 and random() < .75: break
        if len(S) == 1: S *= randrange(1, (5 - int(dif**.5)))
        elif len(S) == 2:
            r = random()
            if dif < 7 and r < .55:
                if r < .3: S *= 2
                else: S = S * 2 + [ S[0] ]
            elif dif < 9 and r > .7: S.append(S[0])
        else:
            r = random()
            if dif < 7 and r < .35: S.append(S[0])
            elif dif < 5 and r < .375: S.extend([ S[0], S[1] ])
        self.quest = S
        self.adjust()

    def adjust(self):
        # The list is sorted by CR and grouped by hash(name)
        # hash is used rather than name in order to not always get alphabetic order
        self.quest.sort(
                key = lambda m: (m["challenge_rating"], hash(m["name"])))
        G = groupby(self.quest, lambda m: m["name"])
        d, s = [], 0
        for name, grp in G:
            grp = list(grp)
            q = len(grp)
            cr = grp[0]["challenge_rating"]
            s += cr * q
            if cr == 0.125: cr = "1/8"
            elif cr == 0.25: cr = "1/4"
            elif cr == 0.5: cr = "1/2"
            else: cr = str(cr)
            m = (str(q) + "x ") if q > 1 else ""
            d.append(m + name + " (CR " + cr + ")")
        self.display  = ", ".join(d)
        self.total_cr = s

    def __repr__(self):
        return self.display

    def __str__(self):
        return self.display

    @staticmethod
    def build_quest(nbr, threshold, epic=True):
        """
        Build a special quest by creating 'nbr' quests, sorting them
        and cumulating some until reaching 'threshold'.
        Quest is epic (huge monsters) or creepy (epic=False)
        """
        Q = [ Quest() for _ in range(nbr) ]
        Q.sort(key=lambda q: q.total_cr, reverse = epic)
        cr, quest = 0, []
        for q in Q:
            cr += sum(m["challenge_rating"] for m in q.quest)
            quest.extend(q.quest)
            if cr >= threshold: break
        Q = Q[0]
        Q.quest = quest
        Q.adjust()
        return Q


### Alter monster_list
### ==================
from random import randrange

def parse_dice(hd):
    i = hd.index("d")
    a, b = int(hd[:i]), int(hd[i+1:])
    return lambda: sum( randrange(1, b+1) for _ in range(a) )

# Add 'hit_func' a lambda that computes "hit_dice"
d = { hd: parse_dice(hd) for hd in { m["hit_dice"] for m in monster_list } }
for m in monster_list: m["hit_func"] = d[m["hit_dice"]]

# Add 'attacks' from "actions" containing at least "damage_dice"
# (eg "attack_bonus": 7, "damage_dice": "2d6", "damage_bonus": 4)
# damage_dice is either some "7d8" or some "4d6 + 5d8"

def parse_dice2(hd):
    if "+" in hd:
        k = hd.index("+")
        d1, d2 = hd[:k].strip(), hd[k+1:].strip()
        i, j = d1.index("d"), d2.index("d")
        a1, b1 = int(d1[:i]), int(d1[i+1:])
        a2, b2 = int(d2[:j]), int(d2[j+1:])
        return lambda: ( sum( randrange(1, b1+1) for _ in range(a1) )
                       + sum( randrange(1, b2+1) for _ in range(a2) ) )
    else: return parse_dice(hd)

d = dict()
for m in monster_list:
    l = []
    m["attacks"] = l
    if "actions" not in m: continue  # Frog // Sea Horse
    for a in m["actions"]:
        a = dict(a)
        if "damage_dice" in a:
            if a["damage_dice"] not in d:
                d[a["damage_dice"]] = parse_dice2(a["damage_dice"])
            l.append(a)
            l[-1]["damage_dice"] = d[a["damage_dice"]]
            if "attack_bonus" not in a: a["attack_bonus"] = 0
            if "damage_bonus" not in a: a["damage_bonus"] = 0
