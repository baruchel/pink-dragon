# -*- coding: utf-8 -*-

from .weapons import melee_weapons as weapons, armors
from random import random, sample, randrange
from functools import reduce

# cost is renormalized to cc (Change standard : 1 gp = 10 sp = 100 cp)
#
#     "cost": 10    instead of   "cost": { "quantity": 1, "unit": "sp" }

__gamma__ = -.25  # adjust probability of items

class Shop:
    def __init__(self, game):
        self.game = game
        self.network = game.network
        self.tick_cnt = 0
        weapons.sort(key = lambda e: e["cost"])
        armors.sort(key = lambda e: e["cost"])
        self.items = weapons + armors # e["equipment_category"])
        for e in range(len(self.items)):
            self.items[e]["index"] = e
        self.total_weight = sum( e["cost"]**__gamma__ for e in self.items )
        self.available = [ 0 ] * len(self.items)
        for _ in range(12):
            e = self.random_item()
            self.available[e["index"]] += 1
        self.display_items()


    def random_item(self):
        r = random() * self.total_weight
        c = 0
        for e in self.items:
            c += e["cost"]**__gamma__
            if r < c: return e
        return self.items[-1]


    def tick(self): # one tick every 45 minutes
        self.tick_cnt = (self.tick_cnt + 1) & 31 # modulo 32, restart every 24h
        if self.tick_cnt & 1:
            self.replace_item( 1 + (self.tick_cnt == 17) )
        self.display_items()

    def display_items(self):
        # self.items = weapons + armors (See e["equipment_category"])
        L = [ (cnt, e) for cnt, e in zip(self.available, self.items)
                if cnt > 0 ]
        L = [ ( ("[" + str(e["index"]) + "] ")
                   if e["equipment_category"] == "Weapon"
                   else ("{" + str(e["index"]) + "} ") )
                + e["name"] + " (cost: "
                + str(e["cost"]) + ")" for cnt, e in L ]
        self.network.broadcast( self.network.header_shop
                + " Currently available at Absalom's store:")
        s, t = [], ""
        for e in L:
            if len(t) + len(e) > 450:
                s.append(t)
                t = e
            else: t = t + (", " if t else "") + e
        s.append(t)
        for e in s:
            self.network.broadcast(self.network.header_shop + " " + e)

    def replace_item(self, nbr):
        L = [ cnt * [e] for cnt, e in zip(self.available, self.items) ]
        L = reduce(list.__add__, L)
        for e in sample(L, nbr):
            self.available[ e["index"] ] -= 1
            k = self.random_item()
            self.available[ k["index"] ] += 1

    def buy(self, user, code_item):
        if code_item < 0 or code_item >= len(self.available): return
        if self.available[code_item]:
            if user.money >= self.items[code_item]["cost"]:
                if user.buy(self.items[code_item]):
                    self.available[code_item] -= 1
                    self.complete()
                    self.network.broadcast( self.network.code_bold
                            + user.nick + self.network.code_reset
                            + " has bought a "
                            + self.items[code_item]["name"]
                            + " (cost: " + str(self.items[code_item]["cost"])
                            + ")." )
                    self.network.reply("You have bought a "
                            + self.items[code_item]["name"]
                            + " (cost: " + str(self.items[code_item]["cost"])
                            + "). Remaining money = " + str(user.money) + ".")
                    # self.display_items()
                else: # already owned
                    self.network.reply("You already have a "
                            + self.items[code_item]["name"] + "!")
            else:
                self.network.reply(
                        "You don't have enough money for buying a "
                        + self.items[code_item]["name"]
                        + " (cost: " + str(self.items[code_item]["cost"])
                        + ")!")
        else:
            self.network.reply(
                    "There is currently no "
                    + self.items[code_item]["name"]
                    + " in the store!")

    def complete(self):
        n = sum(self.available)
        nbr_min, nbr_max = 0, 2
        if n < 8: nbr_min = 1
        if n > 10: nbr_max = 1
        for _ in range(randrange(nbr_min, nbr_max + 1)):
            e = self.random_item()
            self.available[e["index"]] += 1

# a) Le magasin a un stock tournant qui démarre chaque jour à X objets
#    (armes/armures)
# 
#    Si un joueur achète, le magasin recomplète éventuellement en rajoutant
#    entre 0 et k objets selon le nombre actuel, ex:
# 
#      3 obj. restants : entre 1 et 2
#      4 obj. restants : entre 0 et 2
#      5 obj. restants : entre 0 et 2
#      6 obj. restants : entre 0 et 2
#      7 obj. restants : entre 0 et 1
#      (dans cet exemple il y a donc toujours entre 4 et 8 objets)
# 
#    Les objets sont répartis selon une certaine logique : plus un objet
#    est cher plus il est rare.
