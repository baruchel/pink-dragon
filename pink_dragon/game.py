# -*- coding: utf-8 -*-

__version__ = "0.4.2"


from .bboard import BBoard
from .shop import Shop
from .user import User
from .event import Event
from time import time

class Game:
    """
    The GAME is the main object of the whole game; it is initialized
    with the network object as an argument. The network object must
    implement the following methods:

        * broadcast(msg)
        * addEvent(func_callback, delay)
        * reply(msg)    - private reply
    """
    def __init__(self, network):
        global __version__
        self.network = network
        self.users = dict()
        self.killed = False # game can be killed (for restart new game)
        network.broadcast(
            network.code_bold + network.code_red
            + "** The Legend of the Pink Dragon **"
            + network.code_reset
        )
        network.broadcast( "Version " + __version__ + " of the game" )
        self.version = __version__
        self._clock(initialize=True)
        self.start_time = time()
        self.bboard = BBoard(self)
        self.shop = Shop(self)
        self.event = Event(self)

    def _clock(self, initialize=False):
        # The system 'tick' is 675s (11min+15sec.)
        # Four kinds of events are handles; thus
        # each event occur every 45 min.
        if self.killed: return
        self.network.addEvent(self._clock, 675)
        if initialize:
            self.tick_turn = 0
        else:
            self.tick_turn += 1
            if (self.tick_turn & 3) == 1: self.bboard.tick()
            elif (self.tick_turn & 3) == 2: self.event.run()
            elif (self.tick_turn & 3) == 3: self.shop.tick()
            else: self.event.run()
            if (self.tick_turn & 127) == 0: # every 24 hours
                self.purge()

    # Every 7 days, remove players with no quest command
    def purge(self):
        remove = [ u.nick for u in self.users.values()
                if self.tick_turn - u.last_quest_cmd > 896 ]
        for u in remove:
            self.network.broadcast(
                self.network.code_bold + u
                + self.network.code_reset
                + " has left the Realm with no glory.")
            del self.users[u]

    def kill(self):
        self.killed = True
        self.network.broadcast(
                self.network.header_event + " "
            + self.network.code_red
            + "The current game is now stopped."
            + self.network.code_reset
        )

    @staticmethod
    def time_display(sec):
        m, s = divmod(int(sec + .5), 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        return (
            ( (str(d)+"d ") if d > 0 else "" )
            + "%02dh%02dm%02ds" % (h,m,s))


    def buy(self, nick, code_item):
        if nick in self.users:
            self.shop.buy( self.users[nick], code_item)

    def quest(self, nick, code_quest, strategy=None):
        """
        Return message if ascended (else None)
        """
        if nick not in self.users:
            self.network.broadcast( self.network.code_bold
            + nick + self.network.code_reset
            + " has entered the Realm!")
            u = User(self, nick)
            self.users[nick] = u
            self.event.new_user(u)
        user = self.users[nick]
        user.last_quest_cmd = self.tick_turn
        if strategy:
            r = user.set_strategy(strategy)
            if r:
                self.network.reply("Your strategy is now \"" + r + "\".")
        if self.bboard.accept_quest( user, code_quest):
            return ( user.nick 
                    + " has ascended after having played for "
                    + self.time_display(time() - user.creation_time)
                    + " (level " + str(user.level) + " with "
                    + str(user.xp) + " XP)!" )

    def aboutme(self, nick):
        if nick in self.users:
            self.network.reply( self.users[nick].aboutme() )
        else:
            self.network.reply(
                "You are not in the database; start playing by using"
                + " the 'quest' command!" )

    def display_scores(self):
        if len(self.users) == 0: return
        u = [ ( u.level, u.xp, u.nick ) for u in self.users.values() ]
        self.network.broadcast( self.network.code_bold
                + "Best adventurers:" + self.network.code_reset )
        u.sort(reverse=True)
        for level, xp, nick in u:
            self.network.broadcast(
                "| %s (level = %d, XP = %d)" % (nick, level, xp))

__all__ = ['Game']
