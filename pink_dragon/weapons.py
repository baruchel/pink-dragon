# -*- coding: utf-8 -*-

# Two global variables: melee_weapons, armors
# ===========================================
# two units: sc (silver coin) / gc (gold coin)
# cost is renormalized to cc (Change standard : 1 gp = 10 sp = 100 cp)
#
#     "cost": 10    instead of   "cost": { "quantity": 1, "unit": "sp" }

melee_weapons = [{
	"index": 1,
	"name": "Club",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 1,
		"unit": "sp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 4,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/4",
		"name": "Light"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/1"
}, {
	"index": 2,
	"name": "Dagger",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 2,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 4,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 1,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/2",
		"name": "Finesse"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/4",
		"name": "Light"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/8",
		"name": "Thrown"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"throw_range": {
		"normal": 20,
		"long": 60
	},
	"url": "http://www.dnd5eapi.co/api/equipment/2"
}, {
	"index": 3,
	"name": "Greatclub",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 2,
		"unit": "sp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 10,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/9",
		"name": "Two-Handed"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/3"
}, {
	"index": 4,
	"name": "Handaxe",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 5,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/4",
		"name": "Light"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/8",
		"name": "Thrown"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"throw_range": {
		"normal": 20,
		"long": 60
	},
	"url": "http://www.dnd5eapi.co/api/equipment/4"
}, {
	"index": 5,
	"name": "Javelin",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 5,
		"unit": "sp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/8",
		"name": "Thrown"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"throw_range": {
		"normal": 30,
		"long": 120
	},
	"url": "http://www.dnd5eapi.co/api/equipment/5"
}, {
	"index": 6,
	"name": "Light hammer",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 2,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 4,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/4",
		"name": "Light"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/8",
		"name": "Thrown"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"throw_range": {
		"normal": 20,
		"long": 60
	},
	"url": "http://www.dnd5eapi.co/api/equipment/6"
}, {
	"index": 7,
	"name": "Mace",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 5,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 4,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/7"
}, {
	"index": 8,
	"name": "Quarterstaff",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 2,
		"unit": "sp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 4,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/10",
		"name": "Versatile"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"2h_damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"url": "http://www.dnd5eapi.co/api/equipment/8"
}, {
	"index": 9,
	"name": "Sickle",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 1,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 4,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/4",
		"name": "Light"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/9"
}, {
	"index": 10,
	"name": "Spear",
	"equipment_category": "Weapon",
	"weapon_category:": "Simple",
	"weapon_range": "Melee",
	"category_range": "Simple Melee",
	"cost": {
		"quantity": 1,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 4,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 3,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/8",
		"name": "Thrown"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/10",
		"name": "Versatile"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"throw_range": {
		"normal": 20,
		"long": 60
	},
	"2h_damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"url": "http://www.dnd5eapi.co/api/equipment/10"
}, {
	"index": 15,
	"name": "Battleaxe",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 10,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 4,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/10",
		"name": "Versatile"
	}],
	"2h_damage": {
		"dice_count": 1,
		"dice_value": 10,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"url": "http://www.dnd5eapi.co/api/equipment/15"
}, {
	"index": 16,
	"name": "Flail",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 10,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [],
	"url": "http://www.dnd5eapi.co/api/equipment/16"
}, {
	"index": 17,
	"name": "Glaive",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 20,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 10,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 6,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/3",
		"name": "Heavy"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/6",
		"name": "Reach"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/9",
		"name": "Two-Handed"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/17"
}, {
	"index": 18,
	"name": "Greataxe",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 30,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 12,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 7,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/3",
		"name": "Heavy"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/9",
		"name": "Two-Handed"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/18"
}, {
	"index": 19,
	"name": "Greatsword",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 50,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 2,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 6,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/3",
		"name": "Heavy"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/9",
		"name": "Two-Handed"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/19"
}, {
	"index": 20,
	"name": "Halberd",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 20,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 10,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 6,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/3",
		"name": "Heavy"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/6",
		"name": "Reach"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/9",
		"name": "Two-Handed"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/20"
}, {
	"index": 21,
	"name": "Lance",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 10,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 12,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 6,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/6",
		"name": "Reach"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/7",
		"name": "Special"
	}],
	"special": ["You have disadvantage when you use a lance to attack a target within 5 feet of you. Also, a lance requires two hands to wield when you aren’t mounted."],
	"url": "http://www.dnd5eapi.co/api/equipment/21"
}, {
	"index": 22,
	"name": "Longsword",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 15,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 3,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/10",
		"name": "Versatile"
	}],
	"2h_damage": {
		"dice_count": 1,
		"dice_value": 10,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"url": "http://www.dnd5eapi.co/api/equipment/22"
}, {
	"index": 23,
	"name": "Maul",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 10,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 2,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 10,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/3",
		"name": "Heavy"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/9",
		"name": "Two-Handed"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/23"
}, {
	"index": 24,
	"name": "Morningstar",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 15,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 4,
	"properties": [],
	"url": "http://www.dnd5eapi.co/api/equipment/24"
}, {
	"index": 25,
	"name": "Pike",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 5,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 10,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 18,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/3",
		"name": "Heavy"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/6",
		"name": "Reach"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/9",
		"name": "Two-Handed"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/25"
}, {
	"index": 26,
	"name": "Rapier",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 25,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/2",
		"name": "Finesse"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/26"
}, {
	"index": 27,
	"name": "Scimitar",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 25,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 3,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/2",
		"name": "Finesse"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/4",
		"name": "Light"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/27"
}, {
	"index": 28,
	"name": "Shortsword",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 10,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/2",
		"name": "Finesse"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/4",
		"name": "Light"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/11",
		"name": "Monk"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/28"
}, {
	"index": 29,
	"name": "Trident",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 5,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 6,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 4,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/8",
		"name": "Thrown"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/10",
		"name": "Versatile"
	}],
	"throw_range": {
		"normal": 20,
		"long": 60
	},
	"url": "http://www.dnd5eapi.co/api/equipment/29"
}, {
	"index": 30,
	"name": "War pick",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 5,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/8",
			"name": "Piercing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [],
	"url": "http://www.dnd5eapi.co/api/equipment/30"
}, {
	"index": 31,
	"name": "Warhammer",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 15,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 8,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 2,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/10",
		"name": "Versatile"
	}],
	"2h_damage": {
		"dice_count": 1,
		"dice_value": 10,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/2",
			"name": "Bludgeoning"
		}
	},
	"url": "http://www.dnd5eapi.co/api/equipment/31"
}, {
	"index": 32,
	"name": "Whip",
	"equipment_category": "Weapon",
	"weapon_category:": "Martial",
	"weapon_range": "Melee",
	"category_range": "Martial Melee",
	"cost": {
		"quantity": 2,
		"unit": "gp"
	},
	"damage": {
		"dice_count": 1,
		"dice_value": 4,
		"damage_type": {
			"url": "http://www.dnd5eapi.co/api/damage-types/12",
			"name": "Slashing"
		}
	},
	"range": {
		"normal": 5,
		"long": None
	},
	"weight": 3,
	"properties": [{
		"url": "http://www.dnd5eapi.co/api/weapon-properties/2",
		"name": "Finesse"
	}, {
		"url": "http://www.dnd5eapi.co/api/weapon-properties/6",
		"name": "Reach"
	}],
	"url": "http://www.dnd5eapi.co/api/equipment/32"
} ]


armors = [ {
	"index": 38,
	"name": "Padded",
	"equipment_category": "Armor",
	"armor_category": "Light",
	"armor_class": {
		"base": 11,
		"dex_bonus": True,
		"max_bonus": None
	},
	"str_minimum": 0,
	"stealth_disadvantage": True,
	"weight": 8,
	"cost": {
		"quantity": 5,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/38"
}, {
	"index": 39,
	"name": "Leather",
	"equipment_category": "Armor",
	"armor_category": "Light",
	"armor_class": {
		"base": 11,
		"dex_bonus": True,
		"max_bonus": None
	},
	"str_minimum": 0,
	"stealth_disadvantage": False,
	"weight": 10,
	"cost": {
		"quantity": 10,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/39"
}, {
	"index": 40,
	"name": "Studded Leather",
	"equipment_category": "Armor",
	"armor_category": "Light",
	"armor_class": {
		"base": 12,
		"dex_bonus": True,
		"max_bonus": None
	},
	"str_minimum": 0,
	"stealth_disadvantage": False,
	"weight": 13,
	"cost": {
		"quantity": 45,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/40"
}, {
	"index": 41,
	"name": "Hide",
	"equipment_category": "Armor",
	"armor_category": "Medium",
	"armor_class": {
		"base": 12,
		"dex_bonus": True,
		"max_bonus": 2
	},
	"str_minimum": 0,
	"stealth_disadvantage": False,
	"weight": 12,
	"cost": {
		"quantity": 10,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/41"
}, {
	"index": 42,
	"name": "Chain Shirt",
	"equipment_category": "Armor",
	"armor_category": "Medium",
	"armor_class": {
		"base": 13,
		"dex_bonus": True,
		"max_bonus": 2
	},
	"str_minimum": 0,
	"stealth_disadvantage": False,
	"weight": 20,
	"cost": {
		"quantity": 50,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/42"
}, {
	"index": 43,
	"name": "Scale Mail",
	"equipment_category": "Armor",
	"armor_category": "Medium",
	"armor_class": {
		"base": 14,
		"dex_bonus": True,
		"max_bonus": 2
	},
	"str_minimum": 0,
	"stealth_disadvantage": True,
	"weight": 45,
	"cost": {
		"quantity": 50,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/43"
}, {
	"index": 44,
	"name": "Breastplate",
	"equipment_category": "Armor",
	"armor_category": "Medium",
	"armor_class": {
		"base": 14,
		"dex_bonus": True,
		"max_bonus": 2
	},
	"str_minimum": 0,
	"stealth_disadvantage": False,
	"weight": 20,
	"cost": {
		"quantity": 400,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/44"
}, {
	"index": 45,
	"name": "Half Plate",
	"equipment_category": "Armor",
	"armor_category": "Medium",
	"armor_class": {
		"base": 15,
		"dex_bonus": True,
		"max_bonus": 2
	},
	"str_minimum": 0,
	"stealth_disadvantage": True,
	"weight": 40,
	"cost": {
		"quantity": 750,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/45"
}, {
	"index": 46,
	"name": "Ring Mail",
	"equipment_category": "Armor",
	"armor_category": "Heavy",
	"armor_class": {
		"base": 14,
		"dex_bonus": False,
		"max_bonus": None
	},
	"str_minimum": 0,
	"stealth_disadvantage": True,
	"weight": 40,
	"cost": {
		"quantity": 30,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/46"
}, {
	"index": 47,
	"name": "Chain Mail",
	"equipment_category": "Armor",
	"armor_category": "Heavy",
	"armor_class": {
		"base": 16,
		"dex_bonus": False,
		"max_bonus": None
	},
	"str_minimum": 13,
	"stealth_disadvantage": True,
	"weight": 55,
	"cost": {
		"quantity": 75,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/47"
}, {
	"index": 48,
	"name": "Splint",
	"equipment_category": "Armor",
	"armor_category": "Heavy",
	"armor_class": {
		"base": 17,
		"dex_bonus": False,
		"max_bonus": None
	},
	"str_minimum": 15,
	"stealth_disadvantage": True,
	"weight": 60,
	"cost": {
		"quantity": 200,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/48"
}, {
	"index": 49,
	"name": "Plate",
	"equipment_category": "Armor",
	"armor_category": "Heavy",
	"armor_class": {
		"base": 18,
		"dex_bonus": False,
		"max_bonus": None
	},
	"str_minimum": 15,
	"stealth_disadvantage": True,
	"weight": 65,
	"cost": {
		"quantity": 1500,
		"unit": "gp"
	},
	"url": "http://www.dnd5eapi.co/api/equipment/49"
} ]

for e in melee_weapons:
    e["cost"] = e["cost"]["quantity"] * (
                     { "sp":10, "gp": 100 } )[e["cost"]["unit"]]
for e in armors:
    e["cost"] = e["cost"]["quantity"] * (
                     { "sp":10, "gp": 100 } )[e["cost"]["unit"]]
