###
# Copyright (c) 2020, Thomas Baruchel
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

__channel__ = "#legend"
__network__ = "Tilde.chat"
__url__ = "https://pink-dragon.surge.sh"

from .pink_dragon import Game
from .pink_dragon.networks import IrcNetwork
from time import time
import requests

import supybot.schedule as schedule
import supybot.ircmsgs as ircmsgs


from supybot import utils, plugins, ircutils, callbacks
from supybot.commands import *
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('PinkDragon')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


def announce(plugin, msg): # announcement on a dedicated IRC channel
    global __channel__
    ch = plugin.registryValue('announcements')
    if len(ch) > 0 and ch[0] == "#":
        plugin.log.info("[Pink Dragon] announcing \"" + msg + "\" on " + ch)
        plugin.irc.queueMsg(
                ircmsgs.notice(ch,
                    "\x0306\x02** ANNOUNCEMENT **\x0f"
                    + " /Legend of the Pink Dragon/ on \x02"
                    + __channel__ + "\x0f : " + msg ))


def announce_mastodon(plugin, msg): # announcement on Mastodon
    global __channel__, __network__, __url__
    instance = plugin.registryValue('mastodon_instance')
    token = plugin.registryValue('mastodon_token')
    if token and instance:
        postURL = 'https://' + instance + '/api/v1/statuses'
        header = {'Authorization': 'Bearer {}'.format(token)}
        payload = {'status': ("** ANNOUNCEMENT ** " + msg
            +"\n\n\nLegend of the Pink Dragon at " + __channel__
            + " on " + __network__ + " (an #irc game)."
            + " See the rules at " + __url__ )}
        r = requests.post(postURL, data=payload, headers=header)
        if r.status_code == 200:
            r = r.json()
            plugin.log.info("[Pink Dragon] announcing \"" + msg + "\" on Mastodon"
                    + " at " + r['url'])
            plugin.network.broadcast("Announcement was broadcast by @"
                    + r['account']['username'] + "@" + instance
                    + " on Mastodon at " + r['url'])
        else:
            plugin.log.info("[Pink Dragon] Error " + str(r.status_code)
                    + " when attempting to broadcast message \""
                    + msg + "\" on Mastodon")


class PinkDragon(callbacks.Plugin):
    """An RPG game for IRC."""
    def __init__(self, irc):
        self.__parent = super(PinkDragon, self)
        self.__parent.__init__(irc)
        self.irc = irc
        self.network = IrcNetwork(
            lambda msg:      # broadcast
                irc.queueMsg(ircmsgs.notice(__channel__, msg)),
            lambda msg:      # reply
                self.irc.reply(msg),
            lambda f, delay: # addEvent
                schedule.addEvent(f, time() + delay)
            )
        # schedule.addEvent(lambda: 
        self.log.info("[Pink Dragon] Loading the game")
        self.game = Game(self.network)
        msg = "New game is starting (version " + self.game.version + ")."
        announce(self, msg)
        announce_mastodon(self, msg)

    def quest(self, irc, msg, args, quest, strategy):
        """<somethingWithoutSpaces> [<text>]
        Accept a quest (Hex Number).
        Strategy is optional and may be: random, harsh, rage, wise.
        """
        nick = msg.nick
        self.irc = irc
        msg = self.game.quest(nick, quest, strategy=strategy)
        if msg: # msg is an announcement
            announce(self, msg)
            announce_mastodon(self, msg)
    quest = wrap(quest, ['somethingWithoutSpaces', optional("text"),
                            'private'])

    def buy(self, irc, msg, args, item):
        """<int>
        Buy an item from the store.
        """
        nick = msg.nick
        self.irc = irc
        self.game.buy(nick, item)
    buy = wrap(buy, ['int', 'private'])

    def aboutme(self, irc, msg, args):
        """
        Stats about yourself
        """
        nick = msg.nick
        self.irc = irc
        self.game.aboutme(nick)
    aboutme = wrap(aboutme, ['private'])

    def help(self, irc, msg, args):
        """
        Rudimentary help message
        """
        irc.reply("The game is played by sending private messages to the bot."
          + " Read the topic of the " + __channel__ + " and the documenation at "
        + __url__ )
    help = wrap(help, ['private'])

    def botlist(self, irc, msg, args):
        """
        Command displaying some information, required on the network tilde.chat
        """
        irc.reply("A bot maintained by Absalom and providing a hack'n slash game"
                + " on channel " + __channel__
                + " (rules and commands being available at "
                + __url__ + " ). All commands are sent as private messages.")
    botlist = wrap(botlist)


    # Admin commands
    # ==============

    def bboard(self, irc, msg, args):
        """
        Display the Bulletin Board.
        (Admin command only)
        """
        self.game.bboard.display_quests()
    bboard = wrap(bboard, ['admin', 'private'])

    def killgame(self, irc, msg, args):
        """
        Kill the game
        (Admin command only)
        """
        self.game.kill()
    killgame = wrap(killgame, ['admin', 'private'])

    def restartgame(self, irc, msg, args):
        """
        Restart the game
        (Admin command only)
        """
        if not self.game.killed:
            self.game.kill()
            irc.reply("Had to kill the previous game first")
        self.game = Game(self.network)
        msg = "New game is starting (version " + self.game.version + ")."
        announce(self, msg)
        announce_mastodon(self, msg)
    restartgame = wrap(restartgame, ['admin', 'private'])

    def runevent(self, irc, msg, args, e):
        """[<int>]
        Force a specific event to occur now. Random event is triggered
        if no value is provided.
        (Admin command only)
        """
        self.game.event.run(force=e)
    runevent = wrap(runevent, [optional('int'), 'admin', 'private'])

    def removequest(self, irc, msg, args, quest):
        """<somethingWithoutSpaces>
        Remove a specific quest from the Bulletin Board.
        (Admin command only)
        """
        quest = quest.upper()
        if quest in self.game.bboard.quests:
            self.game.bboard.remove_quest(quest)
        else: irc.reply("Wrong quest ID: " + quest)
    removequest = wrap(removequest, ['somethingWithoutSpaces', 'admin', 'private'])

    def addquest(self, irc, msg, args, level):
        """[<int>]
        Force a new quest with a specific level to be added to BBoard.
        Exact meaning of level is related to code in quest.py
        Random level is chosen if no value is provided.
        (Admin command only)
        """
        self.game.bboard.add_new_quest(display=True, level=level)
    addquest = wrap(addquest, [optional("int"), 'admin', 'private'])

Class = PinkDragon


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
