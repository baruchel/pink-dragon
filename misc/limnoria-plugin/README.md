# Running an instance of the game on IRC

Here are the steps to follow for running your own instance of the game (the Mastodon account can perfectly be shared; ask for a token).

## Installing and running Limnoria

Read the following [documentation](https://limnoria-doc.readthedocs.io/en/latest/use/) for installing the Python bot. Configure it and make it run on the required channel. Don't forget to register its nickname if required.

It is not required to _mute_ the channel but it is probably a good thing to do it:

    /mode #mychannel +m
    /mode #mychannel +v my_limnoria_bot

## Installing the plugin

Then create a new (empty) plugin by reading the [documentation](https://limnoria-doc.readthedocs.io/en/latest/develop/plugin_tutorial.html):

    cd plugins/
    supybot-plugin-create PinkDragon

Remove the two created files `config.py` and `plugin.py` and replace them by the two files in the current repository. Edit `plugin.py` and make some changes in the initial global variables.

Finally copy the `pink_dragon` inner directory at the same level than `config.py` and `plugin.py` (the whole directory must be put there and not only the files in that directory).

## Loading the plugin

Go on IRC with your favorite client and talk to the bot by private message; don't forget to use the `identify` command to be recognized.

If you want some major announcements to be broadcast on another channel on the same server, type:

    config plugins.PinkDragon.announcements #another_channel

If you want some major announcements to be broadcast on Mastodon, either create your own account or (rather) contact me for sharing the existing one (I will send to you a token):

    config plugins.PinkDragon.mastodon_instance botsin.space
    config plugins.PinkDragon.mastodon_token XXXXXXXXXXXXXXXXXXXXXXXXX

You can now run the game:

    load PinkDragon

## Managing the game

Several commands can be sent to the bot for managing the game.

TODO
