# Legend of the Pink Dragon

_A tribute to the venerable [Legend of the Red Dragon](https://en.wikipedia.org/wiki/Legend_of_the_Red_Dragon) as a multi-protocol text messaging system game._

The game is fully protocol-agnostic and can be adapted to many different protocols. As an example, the plugin for the Limnoria IRC bot is provided and is made of a very few lines of code with no change anywhere else.

The game is currently running on IRC: see [https://pink-dragon.surge.sh](https://pink-dragon.surge.sh).
